# Compile fat jar #
gradlew jar

# Run #
java -jar <jar filename> <uri> <period> <max number of workers>

# Example #
java -jar build\libs\itron-1.0-SNAPSHOT.jar http://autoscaleproducer-1.azurewebsites.net/api/meter 300 5