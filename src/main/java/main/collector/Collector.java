package main.collector;

import main.receive.Receive;
import main.wait.Wait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Collector extends TimerTask {

    private static final Logger log = LoggerFactory.getLogger(Collector.class);

    private AtomicInteger currentNumberOfWorkers;
    private int maxNumberOfWorkers;
    private final ConcurrentLinkedQueue<Long> receivedItems;
    private final Receive receive;

    public Collector(final int maxNumberOfWorkers, final Receive receive) {
        this.maxNumberOfWorkers = maxNumberOfWorkers;
        currentNumberOfWorkers = new AtomicInteger(0);
        receivedItems = new ConcurrentLinkedQueue<>();
        this.receive = receive;
    }

    @Override
    public void run() {
        try {
            Optional<Long> receivedItem = receive.receive();
            if(receivedItem.isPresent()) {
                receivedItems.offer(receivedItem.get());
                if ((currentNumberOfWorkers.get() < maxNumberOfWorkers) && (receivedItems.size() > 0)) {
                    Thread waitThread = new Thread(new Wait(receivedItems.poll(), currentNumberOfWorkers));
                    waitThread.start();
                }
            }
        } catch (IOException e) {
            log.error("error collecting", e);
        }
    }
}
