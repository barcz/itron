package main.wait;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

public class Wait implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(Wait.class);

    private long wait;
    private final AtomicInteger counter;

    public Wait(final long wait, final AtomicInteger counter) {
        this.wait = wait;
        this.counter = counter;
    }

    @Override
    public void run() {
        try {
            log.info("current number of workers: {}", counter.get());
            counter.incrementAndGet();
            log.info("starting to wait {} on {}", wait, Thread.currentThread().getName());
            Thread.sleep(wait);
            log.info("stop waiting {} on {}", wait, Thread.currentThread().getName());
        } catch (InterruptedException e) {
            log.error("error during waiting", e);
        } finally {
            counter.decrementAndGet();
        }
    }
}
