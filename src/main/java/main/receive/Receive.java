package main.receive;

import java.io.IOException;
import java.util.Optional;

public interface Receive {

    Optional<Long> receive() throws IOException;
}
