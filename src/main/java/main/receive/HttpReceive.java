package main.receive;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;

public class HttpReceive implements Receive {

    private static final Logger log = LoggerFactory.getLogger(HttpReceive.class);

    private final HttpGet httpGet;

    public HttpReceive(final String uri) {
        httpGet = new HttpGet(uri);
    }

    @Override
    public Optional<Long> receive() throws IOException {
        try(CloseableHttpClient httpClient = HttpClients.createDefault()) {
            CloseableHttpResponse response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == 200) {
                byte[] responseBytes = new byte[1024];
                IOUtils.read(response.getEntity().getContent(), responseBytes);
                long wait = Long.parseLong(new String(responseBytes).trim());
                log.info("received number: {}", wait);
                return Optional.of(wait);
            }
        }
        return Optional.empty();
    }

}
