package main;

import main.collector.Collector;
import main.receive.HttpReceive;
import main.receive.Receive;

import java.util.Timer;

public class Main {

    private Main(String... args) {
        if(args.length < 3) {
            System.out.println("usage: java -jar <jar filename> <uri> <period> <max number of workers>");
            return;
        }

        String uri = args[0];
        long period = Long.parseLong(args[1]);
        int maxNumberOfWorkers = Integer.parseInt(args[2]);

        Timer timer = new Timer();

        Receive httpReceive = new HttpReceive(uri);

        Collector c = new Collector(maxNumberOfWorkers, httpReceive);
        timer.scheduleAtFixedRate(c, 0, period);
    }

    public static void main(String[] args) {
        new Main(args);
    }
}
