package main.collector;

import main.receive.Receive;
import org.junit.Test;

import java.io.IOException;
import java.util.Optional;
import java.util.Timer;

import static org.junit.Assert.assertEquals;

public class CollectorTest {

    @Test
    public void test() throws InterruptedException {
        Timer t = new Timer();
        Collector collector = new Collector(2, new TestReceive());
        t.scheduleAtFixedRate(collector, 0, 1000);
        Thread.sleep(6000);
        t.cancel();
        t.purge();

        int numberOfEvenets = TestAppender.events.size();
        long timeToComplete = TestAppender.events.get(numberOfEvenets - 1).getTimeStamp() - TestAppender.events.get(0).getTimeStamp();
        assertEquals("Time to process { 2000, 2000, 2000 } with 2 threads and 1 second period should be 3 seconds", 3000, timeToComplete, 50);
    }

    public class TestReceive implements Receive {

        private int index = 0;
        private long[] numbers = {2000, 2000, 2000};

        @Override
        public Optional<Long> receive() throws IOException {
            if (index < 3) {
                return Optional.of(numbers[index++]);
            } else {
                return Optional.empty();
            }
        }
    }

}